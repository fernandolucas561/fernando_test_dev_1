package backend

class BootStrap {

    def init = { servletContext ->
        new backend.Company(name: "Coca-Cola", segment: "Drinks").save(failOnError: true)
        new backend.Company(name: "Mazda", segment: "Vehicle").save(failOnError: true)
        new backend.Company(name: "Total Express", segment: "Logistics service").save(failOnError: true)
    }

    def destroy = {
    }
}
