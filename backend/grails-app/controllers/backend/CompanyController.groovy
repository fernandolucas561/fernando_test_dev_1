package backend

class CompanyController {
    CompanyService CompanyService

    def index() {
        def companies = CompanyService.getCompanies()
        respond companies, formats: ['json']
    }
}
