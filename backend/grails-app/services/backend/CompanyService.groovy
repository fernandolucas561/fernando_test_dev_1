package backend

import grails.gorm.transactions.Transactional

@Transactional
class CompanyService {

    def serviceMethod() {

    }

    def getCompanies() {
        return Company.list()
    }
}
