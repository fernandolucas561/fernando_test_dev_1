import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListingCompaniesComponent } from './listing-companies/listing-companies.component';
import { CompaniesService } from './services/companies.service';
import { CompanyFilterComponent } from './company-filter/company-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    ListingCompaniesComponent,
    CompanyFilterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [CompaniesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
