import { Component } from '@angular/core';
import { CompaniesService } from '../services/companies.service';
import { ICompany } from '../interfaces/company.interface';

@Component({
  selector: 'app-listing-companies',
  templateUrl: './listing-companies.component.html',
  styleUrls: ['./listing-companies.component.scss']
})
export class ListingCompaniesComponent {
  companies: ICompany[];

  constructor(private companiesService: CompaniesService) { }

  listCompanies() {
    this.companiesService.list().subscribe(data => this.companies = data);
  }

}
