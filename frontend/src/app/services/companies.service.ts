import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICompany } from '../interfaces/company.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {
  companiesUrl = 'http://localhost:8080/companies';

  constructor(private http: HttpClient) {}

  list(): Observable<ICompany[]> {
    return this.http.get<ICompany[]>(this.companiesUrl);
  }
}
