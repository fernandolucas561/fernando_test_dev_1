import { Component, OnInit } from '@angular/core';

import { CompaniesService } from '../services/companies.service';
import { ICompany } from '../interfaces/company.interface';

@Component({
  selector: 'app-company-filter',
  templateUrl: './company-filter.component.html',
  styleUrls: ['./company-filter.component.scss']
})
export class CompanyFilterComponent implements OnInit {
  companies: ICompany[];
  filteredCompanies: ICompany[];

  constructor(private companiesService: CompaniesService) { }

  ngOnInit(): void {
    this.listing();
  }

  filterCompanies(event: any) {
    const companyStr = event.target.value;

    if (companyStr !== '') {
      const filteredCompanies = this.companies.filter(company => {
        const filterName = company.name.toLowerCase().startsWith(companyStr.toLowerCase());
        const filterSegment = company.segment.toLowerCase().startsWith(companyStr.toLowerCase());
        return filterName || filterSegment;
      });

      this.filteredCompanies = filteredCompanies;
    }
  }

  listing() {
    this.companiesService.list().subscribe(data => this.companies = data);
  }
}
